from keystoneauth1 import exceptions as ks_exc
from keystoneauth1 import loading as keystone        
from oslo_log import log as logging

import collections
import nova.conf

from nova.reaper.utils.exceptions import catch_keystone

LOG = logging.getLogger(__name__)
CONF = nova.conf.CONF

Project = collections.namedtuple(
    "Project", ("id", "name", "preemptible"))

def to_tuple(project):
    """Turns a project dict to a named tuple"""
    # NOTE(ttsiouts): Move to utils...
    return Project(project['id'], 
                   project['name'],
                   project['preemptible'])

class KeystoneClient(object):
    """Responsible for getting projects info"""

    def __init__(self):
        auth_plugin = keystone.load_auth_from_conf_options(
            CONF, 'placement')
        self._session = keystone.load_session_from_conf_options(
            CONF, 'keystone', auth=auth_plugin,
            additional_headers={'accept': 'application/json'})

    @catch_keystone
    def get(self, url):
        """Place a get request to the given url"""
        endpoint_filter={'service_type': 'identity','version': (3, 0)}
        return self._session.get(url, 
                                 endpoint_filter=endpoint_filter, 
                                 raise_exc=False)

    def _get_preemptibles(self):
        """Returns a list of all projects"""
        # Don't check the response here
        projects = []
        url = "/projects/"
        resp = self.get(url)
        if resp.status_code == 200:
            json = resp.json()
            projects = [to_tuple(project) for project in json['projects'] \
                if self._is_preemptible(project)]
        return projects

    def _is_preemptible(self, project):
        """Checks if a project is preemptible
 
        :param project: the project to check for

        Returns the value of the project's preemptible property value if it
        exists, or just False if it doesn't.
        """
        try:
            return project['preemptible']
        except KeyError:
            return False

    def get_preemptible_projects(self):
        """Returns a list of preemtible projects"""
        return self._get_preemptibles()

    def is_project_preemptible(self, project_id):
        """Check if a given project is preemptible
  
        :param project_id: the id of the project to check
        """
        url = "/projects/%s" % project_id
        return self._is_preemptible(self.get(url).json()['project'])
