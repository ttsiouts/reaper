from keystoneauth1 import exceptions as ks_exc
from keystoneauth1 import loading as keystone
from nova.reaper.utils import exceptions
from nova.reaper.utils import miscellaneous as misc
from nova.reaper.utils.exceptions import catch_nova
from oslo_log import log as logging

import nova.conf

LOG = logging.getLogger(__name__)
CONF = nova.conf.CONF

## UTILS?????
#
#Allocation = collections.namedtuple(
#    "Allocation", ("provider", "consumer", "resources"))
#
#Inventory = collections.namedtuple(
#    "Inventory", ("example"))
#
#Resources = collections.namedtuple(
#    "Resources", ("vcpu", "memory", "disk"))
#
#def json_to_allocations(json, consumer):
#    allocations = []
#    for provider, item in json.items():
#        alloc = Allocation(provider,
#                           consumer,
#                           json_to_resource(item["resources"]))
#        allocations.append(alloc)
#    return allocations
#
#def json_to_resource(json):
#    return Resources(json["VCPU"],
#                     json["MEMORY_MB"],
#                     json["DISK_GB"])
## UTILS???????

class PlacementClient(object):
    """Allocations information from the Placement API.

    This class provides a the interface with the Placement API to calculate
    the resources allocated to already existing servers.
    """

    def __init__(self):
        auth_plugin = keystone.load_auth_from_conf_options(
            CONF, 'placement')
        self.placement_client = keystone.load_session_from_conf_options(
            CONF, 'placement', auth=auth_plugin,
            additional_headers={'accept': 'application/json'})
        self.ks_filter = {'service_type': 'placement',
                          'region_name': CONF.placement.os_region_name,
                          'interface': CONF.placement.os_interface}

    @catch_nova
    def get(self, url, version):
        kwargs = {
            'headers': {
                'OpenStack-API-Version': 'placement %s' % version
                },
            }
        return self.placement_client.get(url,
            endpoint_filter=self.ks_filter, raise_exc=False, **kwargs)

    def get_resource_providers(self):
        """Returns all resource providers"""
        url = '/resource_providers'
        response = self.get(url, version='1.10')
        return response.json()['resource_providers']

    def get_allocations(self, consumer):
        """Returns allocations for the provided consumer

        :param consumer: the consumer id get the allocations for
        """
        url = '/allocations/%s' % consumer
        response = self.get(url, version="1.10")
        if response.json()['allocations'] == {}:
            raise exceptions.NotFound()
        json = response.json()
        return misc.json_to_allocations(json["allocations"], consumer)

    def get_provider_inventories(self, resource_provider):
        """Returns the inventories of a given provider

        :param resource_provider: the provider to search for
        """
        url = "resource_providers/%s/inventories" % resource_provider
        response = self.get(url, version="1.10")
        return response.json()["inventories"]

    def get_provider_usages(self, resource_provider):
        """Returns the usages of a given provider

        :param resource_provider: the provider to search for
        """
        url = "resource_providers/%s/usages" % resource_provider
        response = self.get(url, version="1.10")
        return response.json()['usages']
