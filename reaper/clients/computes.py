from nova import compute
from keystoneauth1 import exceptions as ks_exc
from keystoneauth1 import loading as keystone
from oslo_log import log as logging
from nova import exception

from nova.reaper.utils.exceptions import catch_nova

import nova.conf
import time

LOG = logging.getLogger(__name__)
CONF = nova.conf.CONF

class ComputeClient(object):
    """Information from the Compute API

    This class is the interface for the reaper to place requests to the Nova
    API directly.
    """

    def __init__(self, context):
        self.compute_api = compute.API()
        self.context = context

    @catch_nova
    def get_project_instances(self, project_id):
        """Get a project's servers
         
        Returns the servers from the provided project
        """
        opts = {"tenant_id": project_id , "deleted": False}
        elevated = self.context.elevated()
        servers = self.compute_api.get_all(elevated, search_opts=opts)
        # NOTE(ttsiouts): Filter out the unassigned for now. This has to
        # be handled better... (if server.host)
        return [server for server in servers if server.host]

    @catch_nova
    def get_host_instances(self, host_id):
        """Get a project's servers
         
        Returns the servers from the provided host
        """
        opts = {"host": host_id , "deleted": False}
        elevated = self.context.elevated()
        return self.compute_api.get_all(elevated, search_opts=opts)

    @catch_nova
    def delete_instance(self, instance_id):
        """Deletes the given instance

        :param instance: the instance_id to be deleted
        """
        elevated = self.context.elevated()
        instance = self.compute_api.get(elevated,
                                        instance_id)
        self.compute_api.delete(elevated, instance)
        self._wait_until_deleted(instance["uuid"], 10)

    def _wait_until_deleted(self, instance_id, timeout):
        # NOTE(ttsiouts): FIX THIS!!! (timeout), use this polling for now.....
        start = time.time()
        now = start
        while now - start <= timeout:
            try:
                self.compute_api.get(self.context, instance_id)
            except exception.InstanceNotFound:
                break
            now = time.time()
