from oslo_log import log as logging
from reaper import Reaper
from utils.exceptions import ReaperException

LOG = logging.getLogger(__name__)

class Dispatcher(object):
     """The test class to trigger the Reaper directly from Nova scheduler"""
     # NOTE(ttsiouts): This class exists only for the first stage of this
     # example prototype, called directly from the Nova code. The plan is to
     # remove this and have the reaper running as a service outside Nova.

     def __init__(self):
         pass

     def trigger_reaper(self, request, ctxt, new_server):
         """ Trigger the reaper from here! """
         try:
             Reaper(ctxt).free_up_space(request, new_server)
         except ReaperException, e:
             # TODO(ttsiouts): handle failures nicely....
             pass

