from oslo_log import log as logging
from nova import exception as nova_exception
from keystoneauth1.exceptions import base as ks_exception

LOG = logging.getLogger(__name__)

class ReaperException(Exception):
    """Base class for the exceptions thrown by the Reaper"""

    code = 500
    message = "Could not free up resources: "
    reason = "Unknown error occured"

    def __init__(self, reason=None, **kwargs):
        if reason:
           self.reason = reason
        self._log_exception()

    def _log_exception(self):
        LOG.exception(self.message+self.reason)

class NotFound(ReaperException):
     reason = "Resource not found"

class PreemptibleRequest(ReaperException):
    """Thrown when Nova tries to free up space for a preemptible server"""
    reason = "Freeing up space for preemptible servers is not allowed"

def catch_nova(function):
    """Cathing Nova exceptions

    The decorator's purpose is to catch Nova exceptions and map them to their
    Reaper equivalent exceptions.
    """
    # TODO(ttsiouts): Define the expected keystone exceptions and their reaper
    # equivalents....
    def decorator(*args, **kw):
        try:
            return function(*args, **kw)
        except nova_exception.NovaException,e:
            raise e
    return decorator

def catch_keystone(function):
    """Cathing Keystone exceptions

    The decorator's purpose is to catch Keystone exceptions and map them to
    their Reaper equivalent exceptions.
    """
    # TODO(ttsiouts): Define the expected keystone exceptions and their reaper
    # equivalents....
    def decorator(*args, **kw):
        try:
            return function(*args, **kw)
        except ks_exception.ClientException,e:
            raise ReaperException("Generic ")
    return decorator
