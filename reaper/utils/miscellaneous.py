import collections

Allocation = collections.namedtuple(
    "Allocation", ("provider", "consumer", "resources"))

Inventory = collections.namedtuple(
    "Inventory", ("example"))

Resources = collections.namedtuple(
    "Resources", ("vcpu", "memory", "disk"))

def json_to_allocations(json, consumer):
    allocations = []
    for provider, item in json.items():
        alloc = Allocation(provider,
                           consumer,
                           json_to_resource(item["resources"]))
        allocations.append(alloc)
    return allocations

def json_to_resource(json):
    return Resources(json["VCPU"],
                     json["MEMORY_MB"],
                     json["DISK_GB"])

def is_resource_greater(a, b):
    """Compares the given resources"""
    return a.vcpu >= b.vcpu \
           and a.memory >= b.memory \
           and a.disk >= b.disk

def add_resources(a, b):
    return Resources(a.vcpu + b.vcpu,
                     a.memory + b.memory,
                     a.disk + b.disk) 

def subtract_resources(a, b):
    return Resources(a.vcpu - b.vcpu,
                     a.memory - b.memory,
                     a.disk - b.disk) 

def inventory_to_total_resources(inventory):
    vcpu = inventory["VCPU"]
    total_vcpu = get_resource_class_total(vcpu)
    ram = inventory["MEMORY_MB"]
    total_ram = get_resource_class_total(ram)
    disk = inventory["DISK_GB"]
    total_disk = get_resource_class_total(disk)
    return Resources(total_vcpu,
                     total_ram,
                     total_disk)

def get_resource_class_total(resource):
    return resource['allocation_ratio'] * resource['total']
