# Reaper Service Prototype

In the first stage of the Reaper service prototype, Reaper is triggered
directly from the scheduler when no candidate hosts are returned from the
Placement API.

# Preemptible Instances (Projects)

For this prototype, the preemptible instances are the instances that belong to
a project that has the property "preemptible=True" set to it.

# Procedure

## The Reaper initializes a host object for each hypervisor

   Each host object gathers information about the hypervisor that is assigned
   to it. The information is gathered for each host with the following
   procedure:

   - Gathers the preemptible instances running on this host

     From the Compute API fetch the instances running on this host. Then for
     each instance's project_id, checks if the project is preemptible. This
     information is taken from the keystone client that each host object has.

   - Gathers the allocations to the preemptible instances

     From the Placement API the reaper gets the allocations to the instances
     found in the previous step.

   - Calculates the maximum request

     The maximum request is calculated by the inventory of the host by taking
     into consideration the max_unit for each resource class.

   - Calculates the potentially available resources

     The potentially available resources is the amount of resources that will
     be available after culling all the preemptible instances on this host.

    This last to steps are done to evaluate the request that will be received
    by the reaper.

## The reaper gives requests the resources to every host object

   Each host object tries to handle the request from the reaper. The steps
   are the following:

   - Evaluate the request

     By comparing the request with the maximum request and the potentially
     available resource, each host decides if it can handle it.

   - Returns a list of candidate instances to kill

     Each host calulates the combination of the preemptible instances that
     have to be killed, in order to have available resources for hosting the
     new non-preemptible VM.

## The reaper decides which offer to accept

   Each offer from a host, is a list of preemptible instances that need to be
   killed. Currently the offer with the lowest number of instances is being
   chosen. Each of the instances in the selected offer is being killed.

## The reaper returns to the scheduler

   After culling the preemptible instances, the reaper returns to the
   scheduler. Then the scheduler requests again candidate hosts from the
   Placement API.

# Concerns

- When killing a VM, the allocations to that VM are not removed instantly, so
  the reaper has to wait until they are deleted. If not, the create request
  for the new VM, will most probably fail since the host will not be chosen by
  Placement API. For the moment a polling mechanism is implemented to make
  sure that the allocations are deleted before returning.

- The reaper should be able to distinguish which hosts are hosting preemptible
  instance instead of querying every one of them. In case of large cloud
  infrastructures this would be a problem. We are searching for ways to have
  only relevant hosts in the host list of the Reaper. The idea is to listen
  for Create-Delete-Migrate events so that the Reaper's host list will be
  updated accordingly.

