import time

from clients.computes import ComputeClient
from clients.resources import PlacementClient
from clients.projects import KeystoneClient
from host import Host
from oslo_log import log as logging
from utils import exceptions
from utils import miscellaneous as misc


LOG = logging.getLogger(__name__)

class Reaper(object):
    """The reaper functionality.

    The class is responsible for culling preemptible servers on demand. This
    is the main functionality of the preemptible servers feature
    """
    # NOTE(ttsiouts): As a first step the Reaper is directly triggered by
    # Nova scheduler when no valid candidate host is returned from the 
    # Placement API. The plan is to have this service external to Nova code.

    def __init__(self, context):
        self.compute = ComputeClient(context)
        self.keystone = KeystoneClient()
        self.placement = PlacementClient() 
        self.check_context(context)
        self._setup(context)

    def _setup(self, context):
        """Gathers info for all the existing hosts"""
        # NOTE(ttsiouts): For now start with one per host
        hosts = self.placement.get_resource_providers()
        self.hosts = [Host(host, context) for host in hosts]

    def free_up_space(self, resources, new_server):
        """Main functionality of the Reaper

        Gathers info and tries to free up the requested resources.

        :param resources: the resources specification for the spawning server
        :param new_server: the id of the new server
        """
        # NOTE(ttsiouts): This is just a to make the prototype work. Most 
        # likely the way of selecting resources is going to change... 
        request = misc.Resources(resources["VCPU"], resources["MEMORY_MB"],
                                 resources["DISK_GB"])
        candidates = self.get_offers_from_hosts(request)
        selected = self.select_offer(candidates)
        self.cull_preemptible_servers(selected)

    def check_context(self, context):
        """Checks if the provided request is supported

        Checks if the provided context of the request concerns a non
        Preemptible server. Resources will be freed up only when a non
        Preemptible server runs out of space.

        :param context: the context where the project_id is taken from
        """
        # NOTE(ttsiouts): The check is here instead of Nova, in order to 
        # touch as less Nova code as possible.
        if self._is_context_preemptible(context):
            raise exceptions.PreemptibleRequest() 

    def _is_context_preemptible(self, context):
        # NOTE(ttsiouts): As a first step here we have to check the project
        # where the server belongs to. If there the property "preemptible" 
        # exists and its value is True then the requested server is 
        # preemptible and this functionality is not supported.
        return self.keystone.is_project_preemptible(context.project_id)


    def get_offers_from_hosts(self, request):
        """Get offers from hosts

        Each host returns a list of VMs that have to be killed in order to be
        able to host the new server
        """
        offers = filter(None, [host.handle_request(request) \
                               for host in self.hosts])
        if len(offers) == 0:
            raise exceptions.ReaperException()
        return offers

    def select_offer(self, offers):
        """Returns the selected offer

        Currently sorts the list of offers and selects the one with the less
        deletions. A policy should be added here
        """
        offers = sorted(offers, key = lambda x: len(x))
        return offers[0]

    def cull_preemptible_servers(self, selected_instances):
        """Culls a preemptible server

        Through the Nova client, places a request to shut off and then delete
        a preemtpible server.
        """
        for instance in selected_instances:
            self.compute.delete_instance(instance)
            self.wait_until_allocation_is_deleted(instance)

    def wait_until_allocation_is_deleted(self, instance_id, timeout=10):
        """Wait until the allocation is deleted

        Tries to get the allocations of a given instance until the it's not
        found or the timeout exceeds
        """
        # Awful way to wait until the allocation is removed.... add a timeout.
        # Have to live with it for now... 
        # Handle the case of failure...
        start = time.time()
        now = start
        while now - start <= timeout:
            try:
                self.placement.get_allocations(instance_id)
            except exceptions.NotFound:
                 break
            now = time.time()


