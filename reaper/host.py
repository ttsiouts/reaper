import collections
import itertools
import time

from clients.computes import ComputeClient
from clients.resources import PlacementClient
from clients.projects import KeystoneClient
from oslo_log import log as logging
from utils import exceptions
from utils import miscellaneous as misc

LOG = logging.getLogger(__name__)

class Host(object):
    """The class containing a host's information"""

    def __init__(self, json, context):
        self.placement = PlacementClient()
        self.compute = ComputeClient(context)
        self.keystone = KeystoneClient()
        self.uuid = json['uuid']
        self.name = json['name']
        self._populate()

    def _populate(self):
        """Gathers the host's information"""
        self.inventories = self.placement.get_provider_inventories(self.uuid)
        self.usages = self.placement.get_provider_usages(self.uuid)
        self.allocations = self.get_preemptible_allocations()
                                   
    def get_preemptible_allocations(self):
        """Get the allocations to preemptible instances on this host"""
        allocations = []
        for instance in self.compute.get_host_instances(self.name):
            if self.instance_is_preemptible(instance):
                allocations += self.placement.get_allocations(instance.uuid)
        return self._sort_allocations(allocations)

    def instance_is_preemptible(self, instance):
        """Checks if the instance belongs to a preemptible project"""
        return self.keystone.is_project_preemptible(instance.project_id)

    def handle_request(self, requested):
        """Returns a list of candidate instances to cull"""
        if not self.validate_request(requested):
            return None
 
        return self.get_culling_candidates(requested)

    def get_culling_candidates(self, requested):
        """Returns a list of candidate instances to cull"""
        # NOTE(ttsiouts): if ascending order is selected then this needs to be
        # optimized. Maybe use a solver for this problem....
        # Shortest or longest path should be configurable?
        selected = []
        resources = self.available_resources()
        for alloc in self.allocations:
            resources = misc.add_resources(resources, alloc.resources)
            selected.append(alloc.consumer)
            if misc.is_resource_greater(resources, requested):
                return selected
        # NOTE(ttsiouts): If we reach this point then this didn't work
        return None

    def validate_request(self, requested):
        """Checks if the request can be handled by this host

        Returns True in case the request can be handled by the host
        """
        total = self.get_total_resources()
        virtual = self.virtual_resources()
        return misc.is_resource_greater(total, requested) \
               and misc.is_resource_greater(virtual, requested)

    def _sort_allocations(self, allocations):
        """Sort the allocations

        Depends on how bloody you want things to get :P
        """
        # NOTE(ttsiouts): Make order configurable
        return sorted(allocations, reverse = True, 
                      key = lambda x: (x.resources.vcpu,
                                       x.resources.memory,
                                       x.resources.disk)) 
        
    def virtual_resources(self):
        """Calculate the potentially available resources

        Calculates the available resources after freeing up all the resources
        already allocated to preemptible instances
        """
        available = self.available_resources()
        preemptible = self.get_preemptible_resources()
        
        return misc.add_resources(available, preemptible)

    def get_preemptible_resources(self):
        """Get the resources allocated to preemptible instances"""
        resources = misc.Resources(0,0,0)
        for alloc in self.allocations:
            resources = misc.add_resources(resources, alloc.resources)
        return resources

    def available_resources(self):
        """Get currently available resources on this host"""
        total_resources = self.get_total_resources()
        used_resources = self.get_used_resources()
        return misc.subtract_resources(total_resources, used_resources)

    def get_total_resources(self):
        """Get total resources this host has"""
        return misc.inventory_to_total_resources(self.inventories)

    def get_used_resources(self):
        """Get currently used resources on this host"""
        return misc.Resources(self.usages["VCPU"],
                              self.usages["MEMORY_MB"],
                              self.usages["DISK_GB"])
        
    def get_max_resources(self, request):
        """Get the maximum request

        Returns the maximum request that can be handled by this host
        """
        return misc.Resources(self.inventories["VCPU"]["max_unit"],
                              self.inventories["MEMORY_MB"]["max_unit"],
                              self.inventories["DISK_GB"]["max_unit"])

